import ImageCompressor from "image-compressor.js";
import { blobToDataURL } from "../../util";

function _arrayBufferToBase64(buffer: ArrayBuffer): string {
  var binary = "";
  var bytes = new Uint8Array(buffer);
  var len = bytes.byteLength;

  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }

  return `data:image/jpeg;base64,${window.btoa(binary)}`;
}

type TemplateDocument = {
  width: number;
  height: number;
  src: string;
  alias: string;
};

type TemplateFont = {
  name: string;
  src: string;
  mime: string;
};

type TemplateField = {
  type: string;
  description: string;
  key: string;
  default: string | { [id: string]: string };
  properties: any;
};

type Template = {
  name: string;
  root: string;
  preview: string;
  documents: TemplateDocument[];
  fonts: TemplateFont[];
  fields: TemplateField[];
};

type LanguageTemplate = {
  template: {
    name: string;
    root: string;
    preview: string;
    documents: TemplateDocument[];
    fonts: TemplateFont[];
    fields: TemplateField[];
  };
  data: {
    hashtag: string;
    lang: string;
    first: string;
    second: string;
    date: string;
  };
};

const template: Template = {
  name: "Profile Picture",
  root:
    "https://toolpic.fridaysforfuture.de/sharepic/templates/profile/template.json",
  preview:
    "https://toolpic.fridaysforfuture.de/data/templates/profile/de/preview.png",
  documents: [
    {
      width: 1200,
      height: 1200,
      src: "data/templates/profile/de/document.svg",
      alias: "1:1"
    }
  ],
  fonts: [
    {
      name: "Jost-600",
      src: "fonts/Jost/Jost-600-Semi.ttf",
      mime: "font/truetype"
    },
    {
      name: "Jost-700",
      src: "fonts/Jost/Jost-700-Bold.ttf",
      mime: "font/truetype"
    },
    {
      name: "Jost-400",
      src: "fonts/Jost/Jost-400-Book.ttf",
      mime: "font/truetype"
    }
  ],
  fields: [
    {
      "type": "Selection",
      "description": "Background",
      "key": "pic",
      "default": {
        "data": "data/templates/profile/fr/bg.jpg"
      },
      "properties": {
        "items": [
          {
            "type": "file"
          }
        ]
      }
    },
    {
      "type": "Line",
      "description": "Hashtag",
      "key": "hashtag",
      "default": "#NeustartKlima",
      "properties": {

      }
    },
    {
      "type": "Line",
      "description": "Langugage",
      "key": "lang",
      "default": "de",
      "properties": {

      }
    },
    {
      "type": "Line",
      "description": "Date",
      "key": "date",
      "default": "29. NOVEMBER",
      "properties": {

      }
    },
    {
      "type": "Line",
      "description": "First Red",
      "key": "first",
      "default": "KLIMA",
      "properties": {

      }
    },
    {
      "type": "Line",
      "description": "Second Red",
      "key": "second",
      "default": "STREIK",
      "properties": {

      }
    }
  ]
};

let __apiEndpoint = "https://api.fridaysforfuture.de:65324/emulate";
let __templateUrl =
  "https://dev.maurice-conrad.eu/toolpic/data/templates/profile/template.json";

let __templatesByLanguage: { [id: string]: LanguageTemplate } = {
  de: {
    template,
    data: {
      hashtag: "#NeustartKlima",
      lang: "de",
      date: "29. NOVEMBER",
      first: "KLIMA",
      second: "STREIK"
    }
  },
  en: {
    template,
    data: {
      hashtag: "#HowDareYou",
      lang: "en",
      date: "NOVEMBER 29",
      first: "CLIMATE",
      second: "STRIKE"
    }
  },
  fr: {
    template,
    data: {
      hashtag: "#HowDareYou",
      lang: "fr",
      date: "29 NOVEMBRE",
      first: "CLIMATIQUE",
      second: "GRÈVE"
    }
  },
  es: {
    template,
    data: {
      hashtag: "#HowDareYou",
      lang: "es",
      date: "29 DE NOVIEMBRE",
      first: "CLIMA",
      second: "HUELGA"
    }
  }
};

function sendImageByDataUrl(
  dataUrl: string,
  language: string
): Promise<string> {
  const body = {
    template: __templatesByLanguage[language]
      ? __templatesByLanguage[language].template
      : __templatesByLanguage["en"].template,
    doc: 0,
    data: Object.assign(
      {
        pic: {
          data: dataUrl
        }
      },
      __templatesByLanguage[language].data
    )
  };
  return fetch(__apiEndpoint, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
  })
    .then((res: Response) => res.arrayBuffer())
    .then((buffer: ArrayBuffer) => _arrayBufferToBase64(buffer));
}

async function buildImage(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    new (ImageCompressor as any)(file, {
      checkOrientation: true,
      maxWidth: 600,
      maxHeight: 600,
      minWidth: 0,
      minHeight: 0,
      width: undefined,
      height: undefined,
      quality: 0.7,
      beforeDraw: null,
      drew: null,
      success: (result: Blob) => {
        const dataUrl = blobToDataURL(result);
        resolve(dataUrl);
      }
    });
  });
}

export async function backendBuildImageUrl(language: string, file: File) {
  const dataUrl = await buildImage(file);
  return sendImageByDataUrl(dataUrl, language);
}
