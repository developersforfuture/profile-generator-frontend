import { EXIFOrientation } from "../../../util";
import { ImageLayer, drawImage } from "./ImageLayer";
import { LayerDrawer } from "./Layer";

export type SquareImageLayer = {
  type: "IMAGE_SQUARE";
  offset: number;
  size: number;
  src: string;
  cornerCutout?: number;
  rotation: EXIFOrientation;
  zoomFactor?: number;
};

export const drawSquareImage: LayerDrawer<SquareImageLayer> = (
  ctx,
  layer,
  width,
  height
) => {
  return drawImage(ctx, squareImageLayerToImageLayer(layer), width, height);
};

/**
 * Converts a SquareImageLayer to an ImageLayer
 * @param layer
 */
function squareImageLayerToImageLayer(layer: SquareImageLayer): ImageLayer {
  return {
    type: "IMAGE",
    offsetX: layer.offset,
    offsetY: layer.offset,
    sizeX: layer.size,
    sizeY: layer.size,
    src: layer.src,
    cornerCutout: layer.cornerCutout,
    rotation: layer.rotation,
    zoomFactor: layer.zoomFactor
  };
}
