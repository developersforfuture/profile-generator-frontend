import { EXIFOrientation } from "../../util";
import { CanvasLayer } from "./Layers";
import { TemplateRenderFunction } from "./clientGenerator";

export type AvatarOptions = {
  width: number;
  height: number;
  template: string;
  src: string;
  srcRotation: EXIFOrientation;
  srcOffsetX: number;
  srcOffsetY: number;
  zoomFactor: number;
  borderWidth: number;
};

export const squareImageAvatar: TemplateRenderFunction = function({
  template,
  width,
  height,
  borderWidth,
  src,
  srcRotation,
  zoomFactor
}: AvatarOptions): CanvasLayer[] {
  const BASE_SIZE = 1200;

  const size = Math.max(width, height);

  return [
    {
      type: "IMAGE_SQUARE",
      offset: borderWidth * (size / BASE_SIZE),
      size: size - borderWidth * 2 * (size / BASE_SIZE),
      src,
      rotation: srcRotation,
      zoomFactor: zoomFactor
    },
    {
      type: "IMAGE_SQUARE",
      offset: 0,
      size: size,
      src: template,
      rotation: 1
    }
  ];
};
