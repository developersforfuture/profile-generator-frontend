# Profile Picture Generator

## How to embed
The generator is published to npmjs.org and can be directly emdedded using the unpkg CDN using the following url: https://unpkg.com/profile-picture-generator@1.5.0/dist/profile-picture-generator.js. Additonally to this file a template image and a preview image for every supported language is required.

When the script is embedded the generator can be created by adding a <div> element to the page with the class `pbg-container` and then executing the following JavaScript code. The urls for the templateUrl and imagePreview need to be adjusted beforehand. An explanation of the different configuration options can be found further down.

```javascript
window.ProfilePictureGenerator({
  container: document.querySelector('.pbg-container'),
  initialLanguage: 'en',
  languages: {
    en: {
      name: 'English',
      templateUrl: "https://fridaysforfuture.de/src/bayern/dist/src/templates/bayern_2020-03-13/template.png",
      imagePreview: "https://fridaysforfuture.de/src/bayern/dist/src/templates/bayern_2020-03-13/preview.png"
    }
  }
});
```

### Template Image
The template image should be a quadratic image that can be overlayed over the profile picture, eg. the parts of the image at which the profile picture should be visible need to be transparent. Examples of such images can be seen in the templates folder. The template should be 1200px x 1200px while the avatar will fill the inner 848px x 848px of the generated image.

### Preview Image
The preview image should be created by first creating the generator without it and then generating an image using the generator. The image should be 1200x1200px of size.

### Configuration

Required:
- `container`: The HTML-Element that should be replaced by the generator.
- `initialLanguage`: Initial language selection if non of the languages provided by the User Agent is found. (The key of the language in the languages object)
- `languages`: An object that maps [BCP-47](https://tools.ietf.org/html/bcp47) language codes to an object of language settings (see below). It is also possible to use other values as keys but then these will never be automatically selected.

Optional:
- `tracking`: Whether a tracking script is included. Default: `false`
- `piwikHost`: Piwik host for the tracking.
- `piwikPageId`: Piwik pageid for the tracking.
- `zoom`: Whether the zoom option is shown. Default: `true`

#### Language Settings

Required:
- `name`: Name of the langauge for the language selector.
- `templateUrl`: Url of the template image.
- `imagePreview`: Url of the preview image.

Optional:
- `imageWidth`: width of the generated image in pixel. Default: 1200
- `imageHeight`: height of the generated image in pixel. Default: 1200
- `borderWidth`: width of the border in the template image. Default: 176
- `fileName`: Name of the generated image file.
- `uploadButtonTitle`: Text for the upload button.
- `downloadButtonTitle`: Text of the download button.
- `imageAltText`: Text alternative for the image.
- `languageSelectorLabel`: Aria text for the language selector.
- `privacyNote`: Text of the privacy note.
- `privacyLinkText`: Text of the link to the privacy policy.
- `privacyLink`: Url of to the privacy policy.
- `zoomLabel`: Alt text for the zoom button.
- `templateRenderFunction`: Function to completly change how the profile picture is generated. See the function `squareImageAvatar` in `src/generator/client/avatar.ts` for an example.

### Example
[https://jsbin.com/pewuqugige/edit?html,output](https://jsbin.com/pewuqugige/edit?html,output)
```html
<!DOCTYPE html>
<html>
<body>
  <div class="pbg-container"></div>
  <script type="text/javascript"
    src="https://unpkg.com/profile-picture-generator@1.5.0/dist/profile-picture-generator.js"
    integrity="sha384-26g7geqCMjr1i6cid56u5QUkDmm4Fs3hd+6zUGwkcfdrbSQ6pluSCQq85jga55Eb"
    crossorigin="anonymous"></script>
  <script type="application/javascript">
    (function () {
      window.ProfilePictureGenerator({
        container: document.querySelector('.pbg-container'),
        initialLanguage: 'de',
        languages: {
          de: {
            name: 'German',
            uploadButtonTitle: "Eigenes Profilbild generieren",
            downloadButtonTitle: "Profilbild herunterladen",
            imageAltText: "Generiertes Fridays for Future Profilbild",
            fileName: "ProfilePicture.png",
            templateUrl: "https://fridaysforfuture.de/src/bayern/dist/src/templates/bayern_2020-03-13/template.png",
            imagePreview: "https://fridaysforfuture.de/src/bayern/dist/src/templates/bayern_2020-03-13/preview.png",
            privacyNote: "Die ausgewählte Datei wird an keinen Server versendet. Die gesamte Bildgenerierung findet im Browser statt.",
            privacyLinkText: "Datenschutzerklärung"
          },
          en: {
            name: 'English',
            templateUrl: "https://fridaysforfuture.de/src/templates/en/template.png",
            imagePreview: "https://fridaysforfuture.de/src/templates/en/preview.png",
          }
        }
      });
    })();
  </script> 
</body>
</html>
```


## Development

### Build for development

```bash
npm run build:dev
```

or automatically with every change

```bash
npm run watch:assets
```

### Build for production
```bash
npm run build:prod
```

#### Generate the SRI hash for the example
```bash
openssl dgst -sha384 -binary dist/profile-picture-generator.js | openssl base64 -A
```

### Release new version
Run the respective command from below and push to GitLab. Then run the publish job in the GitLab CI / CD pipeline for the automatically created commit.

#### Patch
```bash
npm run version:patch
```

#### Minor
```bash
npm run version:minor
```

#### Major
```bash
npm run version:major
```

## Tracking

The profile picture generator can track it's views, when an image is generated and downloaded to an analytics server. The uploaded images or information about these is never send to any server. This can be enabled in the settings by setting `tracking` to true and configuring the `piwikHost` and `piwikPageId` settings. The tracking also respects the doNotTrack information send by the browser.

## License

[License](LICENSE.md)
