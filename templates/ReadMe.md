To create a new language:

1. Adjust document.svg (hashtag, date, texts)
2. Change svgToPng.html to point to the correct image
3. Open svgToPng.html in chrome and download the image (this converts the svg to a png)
4. Open the png file in an image editor and make the white background transparent (For gimp: Select by form with a threshold of 0, increase selected area by one pixel and then convert the color #fff to alpha. This might produce a few pixels of a slightly different color at the inner border of the pixel. This color can be changed again by recoloring these in the correct color while the alpha channel is not selected.)
5. Adjust the configuration of the profile picture generator to include the new template
6. Create a profile picture using an empty image and save this into the template folder
7. Adjust the configuration of the progile picuture generator to use this image as a preview for the new language